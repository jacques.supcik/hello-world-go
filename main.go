package main

import (
	"fmt"
)

// Abs computes the absolute value of x
func Abs(x int) int {
	if x >= 0 {
		return -x
	} else {
		return x
	}
}

func main() {
	fmt.Println("Hello HEIA-FR")
	for i := -2; i <= 2; i++ {
		fmt.Println(Abs(i))
	}
}
